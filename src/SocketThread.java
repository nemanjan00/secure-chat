import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.ArrayList;

import java.util.Base64.Decoder;
import java.util.Base64.Encoder;

public class SocketThread implements Runnable {
	Socket socket;
	SecureChat secureChat;

	PublicKey publicKey;

	public String username;

	public SocketThread(Socket socket, SecureChat secureChat){
		this.socket = socket;
		this.secureChat = secureChat;
	}

	public void sendEncrypted(String message) {
		PrintStream out = null;
		try {
			out = new PrintStream(this.socket.getOutputStream(), true);

			BASE64Encoder base64Encoder = new BASE64Encoder();

			out.println(base64Encoder.encode(this.secureChat.key.encrypt(this.publicKey, message)).replaceAll("\n", ""));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		try {
			PrintStream out = new PrintStream(this.socket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));

			out.println(this.secureChat.getUsername());
			String username = in.readLine();

			BASE64Encoder base64Encoder = new BASE64Encoder();
			BASE64Decoder base64Decoder = new BASE64Decoder();

			String key = base64Encoder.encode(this.secureChat.key.getPublicKey().getEncoded());
			key = key.replaceAll("\n", "");

			out.println(key);
			key = in.readLine();

			KeyFactory factory = KeyFactory.getInstance("RSA");

			this.publicKey = factory.generatePublic(new X509EncodedKeySpec(base64Decoder.decodeBuffer(key)));

			System.out.println("Established connection with user " + username + " with key signature: " + key);

			ArrayList<SocketThread> socketThreads = this.secureChat.socketThreads.get(username);
			if(socketThreads == null){
				socketThreads = new ArrayList<>();
				this.secureChat.socketThreads.put(username, socketThreads);
			}

			socketThreads.add(this);

			while(true){
				String message = in.readLine();

				byte[] messageDecoded = base64Decoder.decodeBuffer(message);

				String decrypted = this.secureChat.key.decrypt(messageDecoded);

				System.out.println(username + ": " + decrypted);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			e.printStackTrace();
		}
	}
}
