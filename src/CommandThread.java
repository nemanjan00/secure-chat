import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.ArrayList;

public class CommandThread implements Runnable {
	SecureChat secureChat;

	public CommandThread(SecureChat secureChat) {
		this.secureChat = secureChat;
	}

	@Override
	public void run() {
		PrintStream out = System.out;
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

		out.println("Now, you can enter commands. ");
		out.println("To connect to another user, type \"connect ip port\" (for example: connect 192.168.1.30 1337)");
		out.println("To send message, type \"username message\" (for example: nemanjan00 hello");

		String line;

		try {
			while(true){
				line = in.readLine();

				String command = line.split(" ")[0];

				if(command.toLowerCase().equals("connect")){
					String[] params = line.split(" ");

					try {
						Socket socket = new Socket(params[1], Integer.parseInt(params[2]));

						SocketThread socketThread = new SocketThread(socket, this.secureChat);

						new Thread(socketThread).start();
					} catch (IOException e) {
						e.printStackTrace();
					}
				} else {
					String username = line.split(" ")[0];
					String message = line.substring(username.length());

					ArrayList<SocketThread> socketThreads = this.secureChat.socketThreads.get(username);

					if(socketThreads == null) {
						System.out.println("User " + username + " not connected! ");
					} else {
						for(SocketThread socketThread: socketThreads){
							socketThread.sendEncrypted(message);
						}
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
