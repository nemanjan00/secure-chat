import javax.crypto.Cipher;
import java.security.*;

public class KeyGenerator {
	private PrivateKey privateKey;
	public PublicKey publicKey;

	public boolean generateKeyPair(){
		try {
			KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
			SecureRandom random = SecureRandom.getInstance("SHA1PRNG");

			keyGen.initialize(512, random);

			KeyPair pair = keyGen.generateKeyPair();

			PrivateKey priv = pair.getPrivate();
			PublicKey pub = pair.getPublic();

			this.setPublicKey(pub);
			this.setPrivateKey(priv);
		} catch(NoSuchAlgorithmException e){
			e.printStackTrace();

			return false;
		}

		return true;
	}

	private void setPrivateKey(PrivateKey priv){
		//byte[] key = priv.getEncoded();

		this.privateKey = priv;
	}

	private PrivateKey getPrivateKey(){
		return this.privateKey;
	}

	private void setPublicKey(PublicKey pub){
		//byte[] key = pub.getEncoded();

		this.publicKey = pub;
	}

	public PublicKey getPublicKey(){
		return this.publicKey;
	}

	public byte[] encrypt(PublicKey pub, String message) {
		byte[] cipherText = null;

		try {
			final Cipher cipher = Cipher.getInstance("RSA");

			cipher.init(Cipher.ENCRYPT_MODE, pub);
			cipherText = cipher.doFinal(message.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return cipherText;
	}

	public String decrypt(byte[] message) {
		byte[] decryptedText = null;

		try {
			// get an RSA cipher object and print the provider
			final Cipher cipher = Cipher.getInstance("RSA");

			// decrypt the text using the private key
			cipher.init(Cipher.DECRYPT_MODE, this.getPrivateKey());
			decryptedText = cipher.doFinal(message);

		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return new String(decryptedText);
	}
}
