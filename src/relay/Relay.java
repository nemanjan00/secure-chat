import java.net.*;
import java.io.*;

public class Relay {
	public Relay(){
		try {
			ServerSocket serverSocket = new ServerSocket(1337);

			while(true) {
				Socket socket = serverSocket.accept();

				RelaySocketThread relaySocketThread = new RelaySocketThread(socket, this);

				new Thread(relaySocketThread).start();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	

	public static void main(String args[]){
		new Relay();
	}
}

class RelaySocketThread implements Runnable {
	private Socket socket;
	private Relay relay;

	public RelaySocketThread(Socket socket, Relay relay){
		this.socket = socket;
		this.relay = relay;
	}

	@Override
	public void run() {
		try {
			PrintStream out = new PrintStream(this.socket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

