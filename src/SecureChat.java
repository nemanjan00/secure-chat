import sun.misc.BASE64Encoder;

import java.io.IOException;
import java.io.PrintStream;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.TreeMap;

public class SecureChat {
	String username;
	KeyGenerator key;

	public TreeMap<String, ArrayList<SocketThread>> socketThreads = new TreeMap<>();

	public SecureChat() throws IOException {
		PrintStream out = System.out;
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

		out.println("Please, enter your username: ");
		String username = in.readLine();

		this.setUsername(username);

		out.println("Username set to: " + this.getUsername());

		out.println("Generating RSA keys. ");

		this.key = new KeyGenerator();
		this.key.generateKeyPair();

		BASE64Encoder base64Encoder = new BASE64Encoder();
		out.println("Key signature: " + base64Encoder.encode(this.key.getPublicKey().getEncoded()).replaceAll("\n", ""));

		ServerSocket serverSocket = new ServerSocket(1339);

		CommandThread commandThread = new CommandThread(this);
		new Thread(commandThread).start();

		while(true) {
			Socket socket = serverSocket.accept();

			SocketThread socketThread = new SocketThread(socket, this);

			new Thread(socketThread).start();
		}
	}

	public String getUsername(){
		return this.username;
	}

	public void setUsername(String username){
		this.username = username;
	}

	public static void main(String args[]) {
		try {
			new SecureChat();
		} catch (IOException e){
			e.printStackTrace();
		}
	}
}
